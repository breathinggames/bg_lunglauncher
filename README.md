# LungLauncher
A game for health co-created by commonners of [Breathing Games](http://www.breathinggames.net), and released under free/libre licence.

Developed on [Unity 3D](http://unity3d.com), to be used on a [Firefox](https://www.mozilla.org/en-US/firefox/products/) browser or [Android](https://www.android.com) phone, with a [breathing device](https://gitlab.com/breathinggames/bg/wikis/5-hardware).

**Read the [documentation](https://gitlab.com/breathinggames/bg_lunglauncher/wikis/home).**

Watch the game mechanics on [YouTube](https://www.youtube.com/watch?v=Wzv0PmE46tw).


## In short
Breath out to launch your lung, catch the pumps and avoid the asthma triggers.

## Contributors
Contributors are welcome. To join, contact info (at) breathinggames.net

- Ned Birkin
- Gareth Brown
- ...

